import numpy


class Graph:
    def __init__(self, matrix_in_lst):
        self.matrix = numpy.array(matrix_in_lst)
        self.authority = numpy.array([1 for i in range(len(matrix_in_lst))])
        self.hubs = numpy.array([1 for i in range(len(matrix_in_lst))])

    def one_hit_iteration(self):
        l = int(max(self.hubs)) ** -1
        self.hubs = self.hubs * l
        self.authority = self.matrix.transpose().dot(self.hubs)

        u = float(max(self.authority)) ** -1
        self.authority = self.authority * u

        self.hubs = self.matrix.dot(self.authority)

        print(f"Authority: {list(self.authority)}")
        print(f"Hubs: {list(self.hubs)}")
        print("\n")


def main():
    matrix_in_lst = [[0, 1, 0, 0, 0, 0],
                     [0, 0, 1, 0, 0, 0],
                     [0, 0, 0, 0, 1, 0],
                     [0, 1, 0, 0, 0, 0],
                     [0, 0, 0, 1, 0, 1],
                     [0, 0, 0, 0, 0, 0]]

    graph = Graph(matrix_in_lst)

    for i in range(10):
        graph.one_hit_iteration()


if __name__ == '__main__':
    main()
